#Bookmarks Manager

This project aims to be a mix of two applications I use a lot: [Google Bookmars](https://www.google.com/bookmarks/) and Read Later Fast, a Chrome extension which is not anymore available in Chrome Web Store


Why this merge?

* I want my saved bookmarks to be available in any browser or device I use, so I want it to be a web application, like Google Bookmarks
* I want to be able to freely tag the bookmarks I save, like in Google Bookmarks
* I don't like Google Bookmarks UI very much
* Google Bookmarks UI is not responsive
* I want the feature to close a browser tab and save it with just one click, like Read Later Fast does. This allows me to postpone some readings when I have something more important to do. This way I can keep focused in what I'm doing without resign to not reading the page and without keeping just another tab open. So, I want it to be a browser extension too
* Since around June 2015 the Read Later Fast is not available in Chrome Web Store anymore and, each time I open Chrome, the extension gets unactive because it violates some Chrome Web Store politics. I can't even find his public repository. So, sooner or later, I won't be able to use it anymore and also it seems it's not safe to use it for some reason. Besides, is getting annoying to check back the extension checkbox every time I open Chrome
* I don't like the Read Later Fast UI neither
* I don't like Chrome Bookmarks. It has a nice UI but the UX is awful. Note I'm refering to Chrome Bookmarks, not Google Bookmarks. Anyway, Chrome Bookmarks stores the data locally, so, is not what I looking for
* As far as I know, there is no application that includes all these features altogheter


So, the desired features are:

* A web application to store the bookmarks. I'm interested to save both pending readings (Read Later Fast) and pages I already read and want to come back to them in te future (Google Bookmarks)
* A browser extension connected to the web application to save the current location as a bookmark and close the tab with one click
* Being able to tag the bookmarks (not just a directory hierarchy)
* Being able to distinguish beetween readed and not readed bookmarkss, in order to have a separated reading list


Other optional features:

* Priorities
* Infer page title from bookmark location
* Import / export from similar tools
* Detect and warn when tags names are too similar (probably due to a typo)
* Tags hierarchy. I would like to be able to declare that a tag includes another tag. This way, all uses of the children tag (both past and future uses) means that their ancestor tag are also added. I would be interesting too to recognize this relationships when they were expressed implicitly, which will probably happens with bookmarks imported from Google Bookmars (e.g. If all uses of the tag Van Halen, also includes the tag Music, but there are other bookmarks tagged as Music but not as Van Halen it would mean that the Van Halen tags is an ancestor of Music tag)


Targets

* Finish a first working version of the project ASAP and start to use it,
since it will be really useful for my daily routine
* Practice my english writting
* I courious about the [Literate programming](https://en.wikipedia.org/wiki/Literate_programming) paradigm so I will try to learn it while I build this project using this [tool](https://weavr.co.uk/about.html)
* I used to make all my personal programming projects private. This time, a bit inspired by [this video](https://www.youtube.com/watch?v=0SARbwvhupQ), I want to see what happens if I make it public since the beginning
* I also want to make it public in order to have something nice to show in my CV

How to install (I'm using Ubuntu 14.04):

* Install pip
* Install virtualenv
* Install virtualenvwrapper
* Create virtual environment
* Activate virutal environment
* pip install -r requirements.txt


Any critic or suggestion is welcome, even english grammar corrections.
If somebody knows about an existing application that already does all that please tell me. I don't want to reinvent the

---

##Log

#### Friday July 3, 2015

The first feature I want to implement is a way to extract my bookmarks from Read Later Fast. I'll do this first because it's probably that soon I won't be able to use that extension anymore
I don't realize how this extension stores the data yet, so I guess I will just inspect the HTML and parse it. Luckily all pages are listed in a single page so it will make things easier.  My intention is to use Python as much I can, so I will use some [XPath](https://en.wikipedia.org/wiki/XPath) parser. Second option: Node and jQuery selectors.

#### Monday July 7, 2015

At first I wanted to parse it using XPath through the lxml Python lybrary, but then I realized that that HTML code can't be directly parsed as XML. Looking for a tool to convert HTML to XHTML I found [Beautiful Soup](http://www.crummy.com/software/BeautifulSoup/bs4/doc/). I liked this library so much that I dediced to use it for all the parsing, without any HTML to XHTML convertion nor XPath queries.

#### Tuesday July 8, 2015

I successfully finished a Python script called import_from_read_later_fast.py which takes the name of a file containing the HTML used by Read Later Fast to list the pages in his options tab (note that this HTML is generated dinamically through javascript, so it has to be obtained inspecting the DOM in Chrome development panel instead of View Source) and converts them to a JSON representation. But there I realized that the HTML representation didn't include the dates when each page were saved, so I started to read the js files included in the HTML and try to figure it out how the data is stored. It turns out that it uses WEB SQL. I came up with a tiny javascript snipped, saved in read_later_fast_to_json.js wich can be pasted into the browser js console and logs a json list.
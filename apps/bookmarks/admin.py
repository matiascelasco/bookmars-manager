from django.contrib import admin
from .models import Bookmark
from tags.models import Tag
from django.utils.text import slugify


def add_tag_action_builder(tag):
    def action(modeladmin, request, queryset):
        for bookmark in queryset:
            tag.bookmarks.add(bookmark)
    action.short_description = "Tag selected bookmarks as %s" % tag.name_en
    action.__name__ = str('add_%s_tag' % slugify(tag.name).replace('-', '_'))
    return action


def add_tag_action_tuple_builder(tag):
    action = add_tag_action_builder(tag)
    return (action.__name__, (action, action.__name__, action.short_description))


@admin.register(Bookmark)
class BookmarkAdmin(admin.ModelAdmin):
    list_display = ['title', 'read', 'tag_names', 'timestamp']
    list_filter = ['read', 'tags']
    search_fields = ['title']
    list_per_page = 15

    def tag_names(self, obj):
        return ', '.join([tag.name_en for tag in obj.tags.all()])

    def get_actions(self, request):
        actions = super(BookmarkAdmin, self).get_actions(request)
        actions.update(dict(add_tag_action_tuple_builder(tag) for tag in Tag.objects.all()))
        return actions

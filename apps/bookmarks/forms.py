from django import forms
from django.db import transaction
from .models import Bookmark, Tag


class BookmarkForm(forms.ModelForm):

    class Meta:
        model = Bookmark
        fields = ('url', 'title', 'read', 'description')

    tags = forms.ModelMultipleChoiceField(Tag.objects.all())

    @transaction.atomic
    def save(self, *args, **kwargs):
        bookmark = super(BookmarkForm, self).save(*args, **kwargs)
        bookmark.add_tags(self.cleaned_data['tags'])
        return bookmark

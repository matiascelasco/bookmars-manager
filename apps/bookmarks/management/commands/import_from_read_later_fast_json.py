import json
import datetime
from pytz import utc
from django.core.management.base import BaseCommand
from bookmarks.models import Bookmark


class Command(BaseCommand):
    help = 'Import Read Later Fast bookmarks from read_later_fast.json file'

    def handle(self, *args, **options):

        with open("read_later_fast.json") as f:
            count = len(Bookmark.objects.bulk_create([
                Bookmark(
                    title=x['title'],
                    url=x['url'],
                    read=x['read'],
                    timestamp=utc.localize(datetime.datetime.fromtimestamp(x['date'] / 1000.0)),
                ) for x in json.load(f)
            ]))
            self.stdout.write('%d bookmarks created' % count)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookmarks', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookmark',
            name='title',
            field=models.CharField(unique=True, max_length=64),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='url',
            field=models.URLField(unique=True),
        ),
    ]

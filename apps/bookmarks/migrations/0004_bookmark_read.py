# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookmarks', '0003_bookmark_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmark',
            name='read',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0006_remove_tag_bookmarks'),
        ('bookmarks', '0007_auto_20150710_1606'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmark',
            name='tags',
            field=models.ManyToManyField(related_name='bookmarks', to='tags.Tag', blank=True),
        ),
    ]

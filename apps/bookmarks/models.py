import urllib2
from django.db import models
from django.utils import timezone
from tags.models import Tag
from bs4 import BeautifulSoup


class Bookmark(models.Model):

    @staticmethod
    def infer_title(url):

        soup = BeautifulSoup(urllib2.urlopen(url), "html.parser")
        if soup.title.string:
            return soup.title.string.strip()

    title = models.CharField(max_length=255)
    url = models.URLField(unique=True)
    timestamp = models.DateTimeField(default=timezone.now)
    description = models.TextField(null=True, blank=True)
    read = models.BooleanField()
    tags = models.ManyToManyField(Tag, related_name='bookmarks', blank=True)

    class Meta:
        ordering = ('-timestamp',)

    def __unicode__(self):
        return self.title

    def infered_title(self):
        return Bookmark.infer_title(self.url)

    def add_tags(self, tags):
        for tag in Tag.exclude_redundancies(tags):
            self.tags.add(tag)
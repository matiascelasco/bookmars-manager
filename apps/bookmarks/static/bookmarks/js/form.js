/*globals
    jQuery
*/

var form = (function ($) {

    'use strict';

    var inferTitleUrl;

    function inferTitleAjaxCallback(response) {
        $('form [name=title]').val(response.title);
    }

    function infer(url, callback) {
        $.get(inferTitleUrl, {url: url}, callback);
    }

    function urlChangeHandler() {
        console.log('change');
        infer($(this).val(), inferTitleAjaxCallback);
    }

    return {
        init: function (params) {
            inferTitleUrl = params.infer_title_url;
            $('form [name=url]').change(urlChangeHandler);

        },
    };

}(jQuery));
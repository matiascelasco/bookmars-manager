from django.shortcuts import render, redirect, get_object_or_404
from jsonview.decorators import json_view
from .forms import BookmarkForm
from .models import Bookmark


@json_view
def infer_title(request):
    url = request.GET['url']
    return {
        'title': Bookmark.infer_title(url),
    }


def add(request):
    form = BookmarkForm(request.POST or None)
    bookmark = None
    if request.method == 'POST':
        if form.is_valid():
            bookmark = form.save()
    return render(request, 'bookmarks/add.jinja', {
        'form': form,
        'bookmark': bookmark,
    })


def edit(request, bookmark_id):
    bookmark = get_object_or_404(Bookmark, id=bookmark_id)
    return render(request, "bookmarks/edit.jinja", {
        'bookmark': bookmark,
    })
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save, post_delete
from bookmarks.models import Bookmark


class Snapshot(models.Model):

    read_bookmarks = models.PositiveSmallIntegerField()
    unread_bookmarks = models.PositiveSmallIntegerField()
    comment = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    @classmethod
    def take(cls, comment=''):
        cls.objects.create(
            read_bookmarks=Bookmark.objects.filter(read=True).count(),
            unread_bookmarks=Bookmark.objects.filter(read=False).count(),
            comment=comment,
        )

    def __unicode__(self):
        return "read: %d, unread:%d (%s)" % (
            self.read_bookmarks,
            self.unread_bookmarks,
            self.comment,
        )


@receiver(post_save, sender=Bookmark)
def bookmark_created_snapshot(sender, instance, created, **kwargs):
    if created:
        Snapshot.take("Stats snapshot taken after new (%s) boomark instance was created. Id = %d"
                      % ('read' if instance.read else 'unread', instance.id))


@receiver(pre_save, sender=Bookmark)
def bookmark_edited_snapshot(sender, instance, **kwargs):
    if instance.id:
        prev_read_state = sender.objects.get(id=instance.id).read
        if prev_read_state != instance.read:
            change_desc = "from " + ("unread to read" if instance.read else "read to unread")
            Snapshot.take("Stats snapshot taken after boomark with id = %d"
                          " changed %s" % (instance.id, change_desc))


@receiver(post_delete, sender=Bookmark)
def bookmark_deleted_snapshot(sender, instance, **kwargs):
    Snapshot.take("Stats snapshot taken after boomark with id = %d was deleted" % instance.id)

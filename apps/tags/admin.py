from django.db import models
from django.contrib import admin
from .models import Tag


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):

    list_display = ('name', 'count', 'read_count', 'unread_count')

    def get_queryset(self, request):
        qs = super(TagAdmin, self).get_queryset(request)
        qs = qs.annotate(models.Count('bookmarks'))
        qs = qs.annotate(read_bookmarks__count=models.Sum('bookmarks__read'))
        return qs

    def unread_count(self, obj):
        return obj.bookmarks__count - self.read_count(obj)

    def read_count(self, obj):
        return obj.read_bookmarks__count or 0
    read_count.admin_order_field = 'read_bookmarks__count'

    def count(self, obj):
        return obj.bookmarks__count
    count.admin_order_field = 'bookmarks__count'

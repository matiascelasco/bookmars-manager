# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0002_auto_20150710_0433'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='bookmarks',
            field=models.ManyToManyField(related_name='tags', to='bookmarks.Bookmark', blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0004_auto_20150710_1727'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='name_en',
            field=models.CharField(default='Unknown', max_length=32),
            preserve_default=False,
        ),
    ]

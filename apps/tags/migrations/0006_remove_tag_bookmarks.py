# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0005_tag_name_en'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tag',
            name='bookmarks',
        ),
    ]

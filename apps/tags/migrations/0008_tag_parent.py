# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0007_auto_20150711_0508'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='parent',
            field=models.ForeignKey(blank=True, to='tags.Tag', null=True),
        ),
    ]

from django.db import models
from django.db import transaction
from django.core.urlresolvers import reverse


class Tree(object):

    @staticmethod
    def ancestors(node):
        current = node.parent
        while current is not None:
            yield current
            current = current.parent

    def __init__(self, value):
        self.parent = None
        self.value = value
        self.children = []
        self.level = 0

    def set_parent(self, parent):
        self.parent = parent
        self.parent.children.append(self)
        self.level = self.parent.level + 1

    def is_descendant_of(self, other):
        for ancestor in Tree.ancestors(self):
            if ancestor == other:
                return True
        return False

    def descendants(self):
        for child in self.children:
            yield child
            for descendant in child.descendants():
                yield descendant


class Tag(models.Model):

    name_sp = models.CharField(max_length=32, unique=True)
    name_en = models.CharField(max_length=32)
    parent = models.ForeignKey('self', null=True, blank=True)

    class Meta:
        ordering = ['name_en']

    @classmethod
    def trees_by_id(cls):
        trees_by_id = dict()
        for tag in cls.objects.all():
            trees_by_id[tag.id] = Tree(tag)
        for tree in trees_by_id.values():
            if tree.value.parent_id is not None:
                tree.set_parent(trees_by_id[tree.value.parent_id])
        return trees_by_id

    @classmethod
    def trees(cls):
        return [tree for tree in cls.trees_by_id().itervalues() if tree.parent is None]

    @classmethod
    def trees_as_dict(cls):
        def tree_as_dict(tree):
            count = tree.value.bookmarks.count()
            list_of_pairs = map(tree_as_dict, tree.children)
            if list_of_pairs:
                children, counts = zip(*list_of_pairs)
                total_count_subtree = sum(counts) + count
                name = "%s (%d/%d)" % (tree.value.name, count, total_count_subtree)
            else:
                name = "%s (%d)" % (tree.value.name, count)
                children = []
                total_count_subtree = count

            return {
                'name': name,
                'children': children,
                'url': reverse('tag', args=[tree.value.id]),
            }, total_count_subtree

        trees, counts = zip(*map(tree_as_dict, cls.trees()))
        return {
            'name': "Tags (%d)" % sum(counts),
            'children': trees,
        }

    @classmethod
    def exclude_redundancies(cls, tags):
        final_set = []
        trees_by_id = cls.trees_by_id()
        for tag in tags:
            should_add = True
            tree = trees_by_id[tag.id]
            for other_tag in tags:
                if tag != other_tag:
                    other_tree = trees_by_id[other_tag.id]
                    if other_tree.is_descendant_of(tree):
                        should_add = False
                        continue
            if should_add:
                final_set.append(tag)
        return final_set

    def __unicode__(self):
        return self.name

    @property
    def name(self):
        return self.name_en

    @transaction.atomic
    def set_parent(self, parent):
        # TODO: avoid cycles by checking if parent is not already a descendant
        from bookmarks.models import Bookmark
        for ancestor in [parent] + list(parent.ancestors()):
            for b in Bookmark.objects.filter(tags=self).filter(tags=ancestor):
                b.tags.remove(ancestor)
        self.parent = parent
        self.save()

    def ancestors(self):
        return Tree.ancestors(self)

    def possible_parents(self):
        trees_by_id = Tag.trees_by_id()
        self_tree = trees_by_id[self.id]
        for other in Tag.objects.all():
            other_tree = trees_by_id[other.id]
            if self != other and not other_tree.is_descendant_of(self_tree):
                yield other

    def descendants(self):
        trees_by_id = Tag.trees_by_id()
        self_tree = trees_by_id[self.id]
        for descendant_tree in self_tree.descendants():
            yield descendant_tree.value
from django.shortcuts import render, get_object_or_404, redirect
from .models import Tag
from bookmarks.models import Bookmark


def tree(request):
    return render(request, "tags/tree.jinja", {
        'tree_data': Tag.trees_as_dict(),
        'untagged_count': Bookmark.objects.filter(tags=None).count(),
    })


def tag(request, tag_id):
    tag = get_object_or_404(Tag, id=tag_id)
    return render(request, "tags/tag.jinja", {
        'tag': tag,
    })


def untagged(request):
    return render(request, "tags/untagged.jinja", {
        'bookmarks': Bookmark.objects.filter(tags=None),
    })


def set_parent(request):
    child = get_object_or_404(Tag, id=request.POST['child_id'])
    parent = get_object_or_404(Tag, id=request.POST['parent_id'])
    child.set_parent(parent)
    return redirect('tag', child.id)
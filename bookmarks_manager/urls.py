from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles import views

base_urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ajax/infer-title', 'bookmarks.views.infer_title', name='ajax_infer_title'),
    url(r'^bookmarks/add', 'bookmarks.views.add', name='add_bookmark'),
    url(r'^tags/tree', 'tags.views.tree', name='tree'),
    url(r'^tags/(?P<tag_id>\d+)', 'tags.views.tag', name='tag'),
    url(r'^tags/set-parent', 'tags.views.set_parent', name='set_parent'),
    url(r'^tags/untagged', 'tags.views.untagged', name='untagged'),
    url(r'^bookmarks/(?P<bookmark_id>\d+)', 'bookmarks.views.edit', name='edit_bookmark'),
]

if settings.DEBUG:
    base_urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]

if settings.URL_PREFIX:
    urlpatterns = patterns('', *[
        url('^%s/' % settings.URL_PREFIX[1:], include(base_urlpatterns)),
    ])
else:
    urlpatterns = base_urlpatterns

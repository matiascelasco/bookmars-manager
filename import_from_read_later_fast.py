import sys
import json
from bs4 import BeautifulSoup


links = []

with open(sys.argv[1]) as f:
    soup = BeautifulSoup(f)
    for li in soup.body.select('#items')[0].ul.children:
        title = unicode(li.h3.contents[0])
        url = li.h3.contents[-1].contents[-1].attrs['data-href']
        links.append({
            'title': title,
            'url': url,
        })

print json.dumps(links)

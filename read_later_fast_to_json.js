/*
Open Read Later Fast options tab, open javascript console and paste this snippet.
It will log a json representation of the links to the console
*/


var i, j, links = [];

function fn(read, callback) {
    'use strict';
    return function (tx, ds) {
        for (j = 0; j < ds.rows.length; j += 1) {
            links.push({
                title: ds.rows[j].title,
                url: ds.rows[j].url,
                // time zone fix
                date: Date.parse(ds.rows[j].server_created_at),
                read: read,
            });
        }
        callback();
    };
}

function logJson() {
    console.log(JSON.stringify(links));
}

function loadFromInbox() {
    loadItems('inbox', fn(false, logJson));
}

loadItems('archive', fn(true, loadFromInbox));

